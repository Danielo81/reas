FROM   tomcat:8.5
MAINTAINER Daniel_Dc "diregui@cajaviviendapopular.gov.co"
COPY /target/Reas-1.0-SNAPSHOT.war /usr/local/tomcat/webapps/Reas.war
COPY server.xml /usr/local/tomcat/conf/
